<?php

namespace Alura\Banco\Service;

use Alura\Banco\Model\Autenticavel; 

class Autenticador{

    public function tentaLogin(Autenticavel $autenticavel, string $senha): void{
        
        if(!$autenticavel->podeAutenticar($senha)){
            echo "Ops, Senha Incorreta";
            exit();
        }

        echo "Ok, Usuário Logado no sistema";

    }
}