<?php

namespace Alura\Banco\Model\Conta;

class ContaCorrente extends Conta{ 

    public function transferir(float $valorATransferir, Conta $contaDestino): void{
        if($valorATransferir > $this->saldo){
            echo "Saldo Indiponível";
            return;
        }

        $this->sacar($valorATransferir);
        $contaDestino->depositar($valorATransferir);
    } 

    protected function percentualTarifa(): float
    {
        return 0.05;
    }
}