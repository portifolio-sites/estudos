<?php 

namespace Alura\Banco\Model\Conta;

abstract class Conta
{
    private Titular $titular;

    protected $saldo;

    private static $numeroDeContas = 0;
    
    public function __construct(Titular $titular, $saldo)
    {
        $this->titular = $titular;
        $this->saldo = $saldo;        
        self::$numeroDeContas++;
    }

    public function __destruct()
    {
        self::$numeroDeContas--;
    }

    public function sacar(float $valor): void{

        $tarifa = $valor * $this->percentualTarifa();
        $valorFinal  = $valor + $tarifa;
        if($valorFinal > $this->saldo){
            echo "Saldo indisponível";
            return;
        }

        $this->saldo -= $valorFinal;
    }

    public function depositar(float $valor): void{
        if($valor < 0){
            echo "Valor precisa ser positívo";
            return;
        }

        $this->saldo += $valor;
    }

	/**
	 * 
	 * @return Titular
	 */
	function getTitular(): Titular {
		return $this->titular;
	}

	/**
	 * 
	 * @return mixed
	 */
	function getSaldo(): float {
		return $this->saldo;
	}
    
	/**
	 * 
	 * @return mixed
	 */
	public static function getNumeroDeContas(): int {
		return self::$numeroDeContas;
	}

    abstract protected function percentualTarifa();
}