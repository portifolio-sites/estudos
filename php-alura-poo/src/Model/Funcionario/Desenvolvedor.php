<?php

namespace Alura\Banco\Model\Funcionario;

use Alura\Banco\Model\Autenticavel;

class Desenvolvedor extends Funcionario implements Autenticavel{

    public function sobeDeNivel() 
    {
        return $this->recebeAumento($this->getSalario() * 0.75);
    }

    public function podeAutenticar(string $senha): bool
    {
        return $senha === 'abcd';
    }
}
