<?php 

namespace Alura\Banco\Model\Funcionario;

use Alura\Banco\Model\Autenticavel;
use Alura\Banco\Model\Pessoa;
use Alura\Banco\Model\Cpf;

abstract class Funcionario extends Pessoa{ 

	private $salario;

    public function __construct(string $nome, Cpf $cpf, float $salario)
    {
        parent::__construct($nome, $cpf); 
		$this->salario = $salario;
    }
 
	/**
	 * 
	 * @return mixed
	 */
	public function getSalario():float {
		return $this->salario;
	}

	public function calculaBonificacao(): float{
		return $this->salario * 0.1;
	}

	protected function recebeAumento(float $valor): void{
		if($valor < 0 ){
			echo "Aumento deve ser positivo";
			return;
		}
		
		$this->salario += $valor;
	}
}