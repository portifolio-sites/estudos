<?php 

namespace Alura\Banco\Model;

/**
 * @property-read string $numero
 */
final class Cpf
{ 
    use AcessoPropriedades;
    
    private String $numero;

    public function __construct($numero)
    {
        $numero = filter_var($numero, FILTER_VALIDATE_REGEXP,[
            'options' => [
                'regexp' => '/^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/'
            ]
        ]);

        if($numero === false){
            echo 'CPF inválido';
            exit();
        }

        $this->numero = $numero;
    }

    
	/**
	 * 
	 * @return mixed
	 */
	function getNumero(): string
    {
		return $this->numero;
	}

}