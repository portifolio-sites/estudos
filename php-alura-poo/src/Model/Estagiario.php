<?php 

namespace Alura\Banco\Model;

use Alura\Banco\Model\{Endereco, Cpf};

class Estagiario extends Pessoa{
    private int $cargaHoraria;

    private Endereco $endereco;

    public function __construct(string $nome, Cpf $cpf, Endereco $endereco, int $cargaHoraria)
    {
        parent::__construct($nome, $cpf);
        $this->cargaHoraria = $cargaHoraria;
        $this->endereco = $endereco;
    }

	/**
	 * 
	 * @return int
	 */
	function getCargaHoraria(): int {
		return $this->cargaHoraria;
	}

	/**
	 * 
	 * @return Alura\Banco\Model\Endereco
	 */
	function getEndereco(): Endereco {
		return $this->endereco;
	}
}
