<?php 

namespace Alura\Banco\Model;

/**
 * @property-read string $logradouro
 * @property-read string $numero
 * @property-read string $complemento
 * @property-read string $cidade
 * @property-read string $bairro
 * @property-read string $estado
 * @property-read string $cep
 */
final class Endereco
{	
	use AcessoPropriedades;

    private string $logradouro;

    private string $numero;

    private string $complemento;

    private string $cidade;

    private string $bairro;

    private string $estado;

    private int $cep;
 

    public function __construct(string $logradouro, string $numero, string $complemento, string $cidade, string $bairro, string $estado, int $cep)
    {
        $this->setLogradouro($logradouro)
			 ->setNumero($numero)
			 ->setComplemento($complemento)
			 ->setCidade($cidade)
			 ->setBairro($bairro)
			 ->setEstado($estado)
			 ->setCep($cep);
        
    } 

    /**
	 * 
	 * @return int
	 */
	private function getCep(): int {
		return $this->cep;
	}

	/**
	 * 
	 * @return string
	 */
	private function getEstado(): string {
		return $this->estado;
	}

	/**
	 * 
	 * @return string
	 */
	private function getBairro(): string {
		return $this->bairro;
	}

	/**
	 * 
	 * @return string
	 */
	private function getCidade(): string {
		return $this->cidade;
	}

	/**
	 * 
	 * @return string
	 */
	private function getComplemento(): string {
		return $this->complemento;
	}

	/**
	 * 
	 * @return string
	 */
	private function getNumero(): string {
		return $this->numero;
	}

	/**
	 * 
	 * @return string
	 */
	private function getLogradouro(): string {
		return $this->logradouro;
	}

	/**
	 * 
	 * @param string $logradouro 
	 * @return Endereco
	 */
	private function setLogradouro(string $logradouro): self {

        if(empty($logradouro)){
            echo "Logradouro obrigatório";
            exit();
        }

		$this->logradouro = $logradouro;
        return $this;
	}
	/**
	 * 
	 * @param string $numero 
	 * @return Endereco
	 */
	private function setNumero(string $numero): self {

        if(empty($numero)){
            echo 'Número obrigatório';
            exit();
        }

		$this->numero = $numero; 
        return $this;
	}
	/**
	 * 
	 * @param string $complemento 
	 * @return Endereco
	 */
	function setComplemento(string $complemento): self {
		$this->complemento = $complemento;
		return $this;
	}
	/**
	 * 
	 * @param string $cidade 
	 * @return Endereco
	 */
	function setCidade(string $cidade): self {

        if(empty($cidade)){
            echo "Cidade obrigatório";
            exit();
        }

		$this->cidade = $cidade;
		return $this;
	}
	/**
	 * 
	 * @param string $bairro 
	 * @return Endereco
	 */
	function setBairro(string $bairro): self {

        if(empty($bairro)){
            echo "Bairro obrigatório";
            exit();
        }

		$this->bairro = $bairro;
		return $this;
	}
	/**
	 * 
	 * @param string $estado 
	 * @return Endereco
	 */
	function setEstado(string $estado): self {

        if(empty($estado)){
            echo "Estado obrigatório";
            exit();
        }

		$this->estado = $estado;
		return $this;
	}
	/**
	 * 
	 * @param int $cep 
	 * @return Endereco
	 */
	function setCep(int $cep): self {
 
        if(strlen($cep) != 8){
            echo "Formato inválido, digite apenas números";
            exit();
        }

		$this->cep = $cep;
		return $this;
	}

	public function __toString(): string
	{
		return "{$this->logradouro}, {$this->numero} [{$this->complemento}] {$this->bairro} - {$this->cidade}/{$this->estado} - {$this->cep}";
	}
 
}