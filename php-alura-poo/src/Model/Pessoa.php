<?php 

namespace Alura\Banco\Model;

abstract class Pessoa{
    protected string $nome;    

    protected Cpf $cpf;

    public function __construct(string $nome, Cpf $cpf)
    {
        $this->setNome($nome); 
        $this->cpf = $cpf;
    }
 
	/**
	 * 
	 * @return string
	 */
	protected function getNome(): string {
		return $this->nome;
	}

	/**
	 * 
	 * @return Cpf
	 */
	protected function getCpf(): Cpf {
		return $this->cpf;
	}
 
	/**
	 * 
	 * @param string $nome 
	 * @return Pessoa
	 */
	protected function setNome(string $nome): self {

        if(strlen($nome) < 5){
            echo "Nome precisa ter pelo menos 5 caractes";
            exit();
        }

		$this->nome = $nome;
		return $this;
	}

}
