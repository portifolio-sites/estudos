<?php 

include_once 'autoload.php';

use Alura\Banco\Model\{Endereco, Cpf, Estagiario};
use Alura\Banco\Model\Conta\{Titular, ContaCorrente, ContaPoupanca};
use Alura\Banco\Service\ControladorDeBonificacoes;
use Alura\Banco\Model\Funcionario\{Funcionario,Gerente,Diretor, Desenvolvedor};
use Alura\Banco\Service\Autenticador;

/*
$autenticador = new Autenticador();
$umDiretor = new Diretor('Sinésio', new Cpf('113.145.537-12'),10000);
$autenticador->tentaLogin($umDiretor, '1234');
*/

/*
$umFuncionario = new Desenvolvedor('Sinésio Luiz', new Cpf('113.145.537-12'), 'Desenvolvedor', 1000);
$umFuncionario->sobeDeNivel();

$umGerente = new Gerente('Jordana', new Cpf('113.145.537-10'), 'Gerente', 3000);
$umDiretor = new Gerente('Arthur', new Cpf('113.145.537-11'), 'Diretor', 5000);

$controlador = new ControladorDeBonificacoes();
$controlador->adicionaBonificacaoDe($umFuncionario);
$controlador->adicionaBonificacaoDe($umGerente);
$controlador->adicionaBonificacaoDe($umDiretor);

echo $controlador->getTotalBonificacao();
*/
/***************************************************************************************************************************************** */
$endereco01 = new Endereco('Rua Doutor Porciúncula', '2424', 'Ap 903, bloco 2', 'São Gonçalo', 'Venda da Cruz','RJ','24411006');
$endereco02 = new Endereco('Rua Delfina Salles', '134', '', 'São Gonçalo', 'Covanca','RJ',24411088);
$endereco03 = new Endereco('Rua Almirante Tamandaré', '22', 'Lado impar', 'Nova Cidade', 'Nilópolis','RJ',26530220);

echo $endereco01 . '</br>';
echo $endereco02 . '</br>';
echo $endereco03 . '</br>';
echo $endereco01->logradouro;

/*
$cpf01 = new Cpf('113.145.537-12');

$titular01 = new Titular($cpf01, 'Sinésio', $endereco01);
$conta01 = new ContaPoupanca($titular01, 1000);
$conta01->sacar(100);

$cpf02 = new Cpf('113.145.537-13');

$titular02 = new Titular($cpf02, 'Jordana', $endereco02);
$conta02 = new ContaCorrente($titular02, 2000);

$cpf03 = new Cpf('113.145.537-77');

$titular03 = new Titular($cpf03, 'Arthur', $endereco03);
$conta03 = new ContaCorrente($titular03, 50);

//$conta01->transferir(200, $conta02);
//$conta02->sacar(200);
*/

echo "<pre>";
    //print_r($conta01);
    //print_r($conta02);
    //print_r($conta03);
echo "</pre>";

/*
$funcionario01 = new Funcionario('Sinésio Luiz', new Cpf('113.145.537-99'), 'Desenvolvedor Backend');
echo "<pre>";
    print_r($funcionario01);
echo "</pre>";

$endEstagiario01 = new Endereco('Rua Almirante Tamandaré', '22', 'Lado impar', 'Nova Cidade', 'Nilópolis','RJ',26530220);
$estagiario01 = new Estagiario('Joaquim Bonifácio', new Cpf('110.006.798-04'), $endEstagiario01, 30);
echo "<pre>";
    print_r($estagiario01);
echo "</pre>";


echo PHP_EOL . "Total de Contas: " . ContaCorrente::getNumeroDeContas();
*/